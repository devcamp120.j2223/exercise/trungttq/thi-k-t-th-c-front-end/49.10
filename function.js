

        "use strict";
        var gObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        };

        var gPercent = 0;
        var gGiamGia = 0;
        var gOrderId = null;
        var gId = null;

        $(document).ready(function () {
            /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

            var gOrder = null;
            var gId = null;
            var gOrderId = null;

            var gNAME_COLS = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "chi tiet"];
            const gORDER_ID = 0;
            const gKICH_CO = 1;
            const gLOAI_PIZZA = 2;
            const gLOAI_NUOC_UONG = 3;
            const gTHANH_TIEN = 4;
            const gHO_TEN = 5;
            const gSO_DIEN_THOAI = 6;
            const gTRANG_THAI = 7;
            const gCHI_TIET = 8;

            // định nghĩa table  - chưa có data
            var gUserTable = $("#user-table").DataTable({
                // Khai báo các cột của datatable
                "columns": [
                    { "data": gNAME_COLS[gORDER_ID] },
                    { "data": gNAME_COLS[gKICH_CO] },
                    { "data": gNAME_COLS[gLOAI_PIZZA] },
                    { "data": gNAME_COLS[gLOAI_NUOC_UONG] },
                    { "data": gNAME_COLS[gTHANH_TIEN] },
                    { "data": gNAME_COLS[gHO_TEN] },
                    { "data": gNAME_COLS[gSO_DIEN_THOAI] },
                    { "data": gNAME_COLS[gTRANG_THAI] },
                    { "data": gNAME_COLS[gCHI_TIET] }
                ],
                // Ghi đè nội dung của cột action, chuyển thành button chi tiết
                "columnDefs": [
                    {
                        "targets": gCHI_TIET,
                        "defaultContent": `
          <i class="fas fa-edit text-success pointer" data-toggle="tooltip" title="Sửa đơn hàng"></i>
          <i class="fas fa-trash-alt text-danger pointer" data-toggle="tooltip" title="Xóa đơn hàng"></i>
        `
                    }]
            });

            /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
            onPageLoading();

            // gán click event handler cho button chi tiet
            $("#user-table").on("click", ".info-user", function () {
                onButtonChiTietClick(this); // this là button được ấn
            });

            $(document).on("click", "#btn-filter", function () {
                onBtnFilterClick();
            });

            $(document).on("click", ".info-user", function () {
                onBtnDetailClick(this);
            });

            $(document).on("click", "#btn-insert-order", function () {
                onBtnInsertOrderClick();
            });

            $(document).on("change", "#inp-insert-combo", function () {
                onComboChange(gObjectRequest);
            });

            $(document).on("click", ".confirminsert", function () {
                onBtnCofirmInsertOrderClick();
            });

            $(document).on("click", ".fa-edit", function(){
                onBtnDetailClick(this);
            })

            $(document).on("click", ".confirmupdate", function(){
                onBtnConfirmUpdateClick();
            });

            $(document).on("click", ".fa-trash-alt", function(){
                onBtnDeleteClick(this);
                
            });

            $(document).on("click", "#btn-confirm-delete-order", function(){
                deleteOrder(gId);
            })

            function onBtnDeleteClick(paramElement){
                $("#delete-confirm-modal").modal("show");
                var gOrderId = $(paramElement).closest("tr").find("td:eq(0)").text();
                gId = getIdByOrderId(gOrderId);

            }

            function deleteOrder(paramId){
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + paramId,
                    type: "DELETE",
                    success: function(){
                        alert("Xóa đơn hàng Id: " + paramId + " thành công");
                        location.reload();
                    },
                    error: function(){
                        alert("fail");
                    }

                })
            }

            function onBtnConfirmUpdateClick(){
                var vObjectRequest = {
                    trangThai: "" //3: trang thai open, confirmed, cancel tùy tình huống
                }
                vObjectRequest.trangThai = $("#inp-trangthai").val();
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + gId,
                    type: "PUT",
                    contentType: "application/json",
                    data: JSON.stringify(vObjectRequest),
                    async: "false",
                    success: function(){
                        alert("Cập nhật trạng thái đơn hàng thành công");
                        location.reload();
                    },
                    error: function(){
                        alert("fail");
                    }
                });
            }

            function onBtnDetailClick(paramElement){
                $("#order-modal").modal("show");
                getOrderClickData(paramElement);
            }

            function getOrderClickData(paramElement) {
                var gOrderId = $(paramElement).closest("tr").find("td:eq(0)").text();
                gId = getIdByOrderId(gOrderId);

                console.log(gId);
                console.log(gOrderId);

                getOrderByOrderId(gOrderId);
            }

            function getIdByOrderId(paramOrderId) {
                var vResult = null;
                var vFound = false;
                var vIndex = 0;

                while (!vFound && vIndex < gOrder.length) {
                    if (gOrder[vIndex].orderId == paramOrderId) {
                        vFound = true;
                        vResult = gOrder[vIndex].id;
                    }
                    vIndex++;
                }
                return vResult;
            }

            function getOrderByOrderId(paramOrderId) {
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + paramOrderId,
                    type: "GET",
                    dataType: "json",
                    success: function (Order) {
                        showDataToModal(Order);
                    },
                    error: function (error) {
                        alert("fail");
                    }
                })
            }

            function showDataToModal(paramOrder) {
                $("#inp-madonhang").val(paramOrder.orderId);
                $("#inp-combo option:selected").text(paramOrder.kichCo);
                $("#inp-duongkinh").val(paramOrder.duongKinh);
                $("#inp-suon").val(paramOrder.suon);
                $("#inp-douong option:selected").text(paramOrder.idLoaiNuocUong);
                $("#inp-soluongnuoc").val(paramOrder.soLuongNuoc);
                $("#inp-voucherid").val(paramOrder.idVourcher);
                $("#inp-loaipizza").val(paramOrder.loaiPizza);
                $("#inp-salad").val(paramOrder.salad);
                $("#inp-thanhtien").val(paramOrder.thanhTien);
                $("#inp-giamgia").val(paramOrder.giamGia);
                $("#inp-hoten").val(paramOrder.hoTen);
                $("#inp-email").val(paramOrder.email);
                $("#inp-sdt").val(paramOrder.soDienThoai);
                $("#inp-diachi").val(paramOrder.diaChi);
                $("#inp-loinhan").val(paramOrder.loiNhan);
                $("#inp-trangthai").val(paramOrder.trangThai);
                $("#inp-ngaytaodon").val(paramOrder.ngayTao);
                $("#inp-ngaycapnhat").val(paramOrder.ngayCapNhat);
            }

            function onBtnCofirmInsertOrderClick() {
                getInsertOrderData(gObjectRequest);
                var vValidate = validateData(gObjectRequest);
                if (vValidate) {
                    insertOrder(gObjectRequest);
                }
            }

            function insertOrder(paramOrder) {
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                    type: "POST",
                    contentType: "application/json",
                    async: "false",
                    data: JSON.stringify(paramOrder),
                    success: function () {
                        alert("tạo đơn hàng thành công");
                        location.reload();
                    },
                    error: function () {
                        alert("fail");
                    }
                });
            }

            function getInsertOrderData(paramOrder) {
                paramOrder.kichCo = $("#inp-insert-combo").val();
                paramOrder.duongKinh = $("#inp-insert-duongkinh").val();
                paramOrder.suon = $("#inp-insert-suon").val();
                paramOrder.salad = $("#inp-insert-salad").val();
                paramOrder.loaiPizza = $("#inp-insert-loaipizza").val();
                paramOrder.idVourcher = $("#inp-insert-voucherid").val().trim();
                paramOrder.idLoaiNuocUong = $("#inp-insert-douong").val();
                paramOrder.soLuongNuoc = $("#inp-insert-soluongnuoc").val();
                paramOrder.hoTen = $("#inp-insert-hoten").val().trim();
                paramOrder.thanhTien = $("#inp-insert-thanhtien").val();
                paramOrder.email = $("#inp-insert-email").val().trim();
                paramOrder.soDienThoai = $("#inp-insert-sdt").val().trim();
                paramOrder.diaChi = $("#inp-insert-diachi").val().trim();
                paramOrder.loiNhan = $("#inp-insert-loinhan").val().trim();
                if (paramOrder.idVourcher != "") {
                    checkVoucherId();
                }

            }

            function checkVoucherId(paramVoucherID) {
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail" + "/" + gObjectRequest.idVourcher,
                    type: "GET",
                    dataType: "json",
                    async: false,
                    success: function (voucherObj) {
                        gPercent = voucherObj.phanTramGiamGia;
                        gGiamGia = (gObjectRequest.thanhTien * parseInt(gPercent)) / 100;
                        $("#inp-insert-giamgia").val(gGiamGia);
                    },
                    error: function () {
                        alert("mã giảm giá không tồn tại");
                        $("#inp-insert-giamgia").val(gGiamGia);
                    }
                })
            }

            function validateData(paramOrder) {
                if (paramOrder.kichCo == "0") {
                    alert("Vui lòng chọn combo");
                    return false;
                }
                if (paramOrder.idLoaiNuocUong == "0") {
                    alert("Vui lòng chọn loại nước");
                    return false;
                }
                if (paramOrder.loaiPizza == "0") {
                    alert("Vui lòng chọn loại pizza");
                    return false;
                }
                if (paramOrder.email != "") {
                    if (!paramOrder.email.includes("@")) {
                        alert("Email không hợp lệ");
                        return false;
                    }
                }
                if (paramOrder.soDienThoai == "") {
                    alert("Vui lòng nhập số điện thoại");
                    return false;
                }
                if (paramOrder.diaChi == "") {
                    alert("Vui lòng nhập địa chỉ");
                    return false;
                }
                return true;
            }

            function onComboChange(paramOrder) {
                paramOrder.kichCo = $("#inp-insert-combo").val();
                switch (paramOrder.kichCo) {
                    case "0":
                        $("#inp-insert-duongkinh").val("");
                        $("#inp-insert-suon").val("");
                        $("#inp-insert-soluongnuoc").val("");
                        $("#inp-insert-salad").val("");
                        $("#inp-insert-thanhtien").val("");
                        break;
                    case "S":
                        $("#inp-insert-duongkinh").val("20");
                        $("#inp-insert-suon").val("2");
                        $("#inp-insert-soluongnuoc").val("2");
                        $("#inp-insert-salad").val("200");
                        $("#inp-insert-thanhtien").val("150000");
                        break;
                    case "M":
                        $("#inp-insert-duongkinh").val("25");
                        $("#inp-insert-suon").val("4");
                        $("#inp-insert-soluongnuoc").val("3");
                        $("#inp-insert-salad").val("300");
                        $("#inp-insert-thanhtien").val("200000");
                        break;
                    case "L":
                        $("#inp-insert-duongkinh").val("30");
                        $("#inp-insert-suon").val("8");
                        $("#inp-insert-soluongnuoc").val("4");
                        $("#inp-insert-salad").val("500");
                        $("#inp-insert-thanhtien").val("250000");
                        break;
                };

            }

            function onBtnInsertOrderClick() {
                $("#insert-order-modal").modal("show");
            }


            function onBtnFilterClick() {
                var vOrder = {
                    status: "",
                    pizzaType: ""
                }

                getUserFilter(vOrder);
                console.log(vOrder);
                filterUser(vOrder);
            }

            function getUserFilter(paramOrder) {
                paramOrder.status = $("#select-status").val().trim();
                paramOrder.pizzaType = $("#select-pizza-type").val().trim();

            }

            function filterNullPizzaType(paramOrderObj) {

                if (paramOrderObj.loaiPizza != null) {
                    return paramOrderObj.loaiPizza.toLowerCase();
                }

            }

            function filterNullStatus(paramOrderObj) {

                if (paramOrderObj.trangThai != null) {
                    return paramOrderObj.trangThai.toLowerCase();
                }

            }


            function filterUser(paramOrder) {
                var vOrderFilted = [];

                vOrderFilted = gOrder.filter(function (paramOrderObj) {
                    return ((paramOrder.status == "all" && paramOrder.pizzaType == "all")
                        || (paramOrder.status == "all" && filterNullPizzaType(paramOrderObj) == paramOrder.pizzaType.toLowerCase())
                        || (paramOrder.pizzaType == "all" && filterNullStatus(paramOrderObj) == paramOrder.status.toLowerCase())
                        || (filterNullPizzaType(paramOrderObj) == paramOrder.pizzaType.toLowerCase()
                            && filterNullStatus(paramOrderObj) == paramOrder.status.toLowerCase()));
                });

                if (vOrderFilted.length == 0) {
                    alert("không tìm thấy kết quả");
                }
                else {
                    loadDataToTable(vOrderFilted);
                }


            }

            // hàm chạy khi trang được load
            function onPageLoading() {
                // lấy data từ server
                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                    type: "GET",
                    dataType: 'json',
                    success: function (responseObject) {
                        gOrder = responseObject;

                        //debugger;
                        loadDataToTable(responseObject);
                        console.log(gOrder);
                    },
                    error: function (error) {
                        console.assert(error.responseText);
                    }
                });

                $.ajax({
                    url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
                    type: "GET",
                    success: function (drink) {
                        loadDrinkToSelect(drink);
                    }
                });
            }

            function loadDrinkToSelect(paramDrink) {
                for (var drink of paramDrink) {
                    $("<option>", { text: drink.tenNuocUong, value: drink.maNuocUong }).appendTo($("#inp-insert-douong"));
                }
            }

            /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
            // load data to table
            function loadDataToTable(paramResponseObject) {
                //Xóa toàn bộ dữ liệu đang có của bảng
                gUserTable.clear();
                //Cập nhật data cho bảng 
                gUserTable.rows.add(paramResponseObject);
                //Cập nhật lại giao diện hiển thị bảng
                gUserTable.draw();
            }
        });
